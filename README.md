# Solution for komoot challenge
The solution for the challenge is deployed as an AWS Lambda, uses a DynamoDB Table to store recent registered users, 
and a SQS queue subscribed to the SNS topic providing new user events. Resources are defined using terraform.
The AWS Lambda is written in Kotlin with no framework used, only the aws sdk libraries. JUnit with MockK is used for testing.

## Choices of services
### Why AWS Lambda?
 * The solution is only a small code base
 * The events are expected to be distributed unevenly
 * For this use case, it's fine if event processing takes > 1s (which might happen with AWS Lambda + JVM on cold lambdas)
 * AWS free tier should suffice

### Why DynamoDB
 * It's easy to set up
 * It easily scales to different sizes
 * The stored users is a fairly simple data schema that doesn't need a sophisticated data design
 * It's possible to add a time to live to data, thus automatically deleting users that are not recent anymore  
 * AWS free tier should suffice

### Why SQS
 * It's easy to integrate with SNS and AWS Lambda
 * While it's possible to directly subscribe to SNS, you get a dead letter queue with SQS to replay events if necessary

### Why terraform
 * Works good with aws
 * Makes it easy to reproduce infrastructure (Much better than manual resource creation)

## Overview
On a new event that is added to the queue, the lambda will be invoked. It will retrieve all recent users (with a limit of 100 to prevent timeouts),
shuffle them and take three. Now the push notification sending service is called with the message, the new user id and
the previously selected three recent users. Finally, the new user is written to the DynamoDB table, to be received as a recent user for one of the next new users.
The DynamoDB entries have a time to live, to make sure there are only recent users in the table. 

At this point, one can discuss what a recent user is and this heavily depends on the expected number of new users. 
I assumed that for this challenge scenario, not too many users are posted to the queue, 
so that there should be less than a hundred user in 24h. For a real live scenario, the numbers should be tweaked. 
From the numbers I found on the internet, you have a few hundred new users per hour. In this case, the ttl should be 
less than an hour. The time to live should be in general chosen so that the number of stored users is low. 

## Scaling
A welcome email is one of the first things a new users sees, so this service should be reliable.
Numbers of registered users can have heavy spikes, for example when the app is featured on TV with an audience of millions of people.
The presented solution can handle thousands of concurrent executions (thanks to lambda) and can scale fast (thanks to DynamoDB).
As long as the push notification service can handle the requests, there won't be any bottle neck and up to 430,000 new registration could be handled 
per Minute¹. Should there be a spike that the push notification can't handle and thus throttles the execution time,
SQS saves the day, keeping the messages in the queue until they are processed, preventing them from being discarded.
A replay is possible due to the dead letter queue, making sure every new user gets his message eventually.

[1] A cold lambda takes around 8000ms to execute, a warm lambda around 120ms. So for 1000 concurrent executions and assuming no warm lambda, 
this results in more than 430,000 possible executions for the first minute. 
After that, it will speed up to 500,000 per minute, which results in 720 Mio registration that could be handled per day.

# How to run the code
* To deploy the code, you need terraform installed (I used version 0.14.4) and aws cli configured for the account you want to deploy to.
* Run `./gradlew shadowJar` to compile the code.
* Go to the directoy `terraform` and run `terraform apply`. This will create all necessary resources and deploys the lambda as well.
The lambda is now executed on new events.