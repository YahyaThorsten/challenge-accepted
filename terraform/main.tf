terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region = "eu-west-1"
}

resource "aws_sqs_queue" "user-queue" {
  name = "challenge-accepted-queue"
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.user-queue-dlq.arn
    maxReceiveCount = 4
  })
}

resource "aws_sqs_queue" "user-queue-dlq" {
  name = "challenge-accepted-dead-letter-queue"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "SQSPolicyReceiveSNS",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "sqs:SendMessage",
      "Resource": "arn:aws:sqs:eu-west-1:275226015868:challenge-accepted-queue",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:963797398573:challenge-backend-signups"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sns_topic_subscription" "new-user-subscription" {
  endpoint = aws_sqs_queue.user-queue.arn
  protocol = "sqs"
  topic_arn = "arn:aws:sns:eu-west-1:963797398573:challenge-backend-signups"
}

resource "aws_dynamodb_table" "user-table" {
  hash_key = "userId"
  name = "UserTable"
  write_capacity = 1
  read_capacity = 1
  ttl {
    attribute_name = "ttl"
    enabled = true
  }

  attribute {
    name = "userId"
    type = "S"
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "challenge-accepted-lambda" {
  function_name = "challenge-accepted-lambda"
  handler = "com.witzig.komoot.challenge.Handler"
  role = aws_iam_role.iam_for_lambda.arn
  runtime = "java11"
  memory_size = 512
  timeout = 60
  filename = "../build/libs/challenge-accepted-1.0-SNAPSHOT.jar"
  depends_on = [aws_iam_role_policy_attachment.challenge-accepted-logs,
    aws_cloudwatch_log_group.challenge-logs,]
}

resource "aws_lambda_event_source_mapping" "challenge-accepted-lambda-trigger" {
  event_source_arn = aws_sqs_queue.user-queue.arn
  function_name = aws_lambda_function.challenge-accepted-lambda.function_name
}


resource "aws_cloudwatch_log_group" "challenge-logs" {
  name              = "/aws/lambda/challenge-accepted-logs"
  retention_in_days = 7
}

resource "aws_iam_policy" "lambda-policies" {
  name        = "lambda-logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "sqs:ReceiveMessage",
        "sqs:DeleteMessage",
        "sqs:GetQueueAttributes",
        "dynamodb:Scan",
        "dynamodb:Update",
        "dynamodb:PutItem",
        "dynamodb:UpdateTimeToLive"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "challenge-accepted-logs" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda-policies.arn
}