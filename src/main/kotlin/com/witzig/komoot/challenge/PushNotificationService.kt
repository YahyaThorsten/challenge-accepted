package com.witzig.komoot.challenge

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.witzig.komoot.challenge.dto.User
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

class PushNotificationService(private val pushMessageCreator: PushMessageCreator) {

    private val client = OkHttpClient()
    private val notificationUrl = "https://notification-backend-challenge.main.komoot.net/"

    fun sendPushNotification(newUser: User, recentUsers: List<User>): Boolean {
        println("Send push notification for ${newUser.name}")
        val body = createPushBody(newUser, recentUsers)
        val request = Request
                .Builder()
                .url(notificationUrl)
                .post(body)
                .build()
        val response = client.newCall(request).execute()
        if (!response.isSuccessful) {
            println("Sending notification failed with status ${response.code} and message ${response.message}")
        }
        return response.isSuccessful
    }

    private fun createPushBody(newUser: User, recentUsers: List<User>): RequestBody {
        val pushNotification = pushMessageCreator.createPushNotification(newUser, recentUsers)
        return jacksonObjectMapper()
                .writeValueAsString(pushNotification)
                .toRequestBody("application/json; charset=utf-8".toMediaType())
    }

}

