package com.witzig.komoot.challenge

import com.witzig.komoot.challenge.dto.NewUserEvent
import com.witzig.komoot.challenge.dto.User

class NewUserService(private val pushNotificationService: PushNotificationService,
                     private val recentUserService: RecentUserService) {

    fun handleNewUserEvent(event: NewUserEvent) {
        val newUser = User(id = event.id, name = event.name)
        println("Handle event for user ${event.id}")
        val recentUser = recentUserService.retrieveThreeRandomRecentUsers()

        val pushSuccess = pushNotificationService.sendPushNotification(newUser, recentUser)
        if (!pushSuccess) {
            throw RuntimeException("Could not push notification")
        }
        recentUserService.addRecentUser(newUser)
        println("Successfully processed event for ${event.id}")
    }
}