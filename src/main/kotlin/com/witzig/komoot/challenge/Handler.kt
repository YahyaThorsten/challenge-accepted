package com.witzig.komoot.challenge

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.SQSEvent
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.witzig.komoot.challenge.dto.NewMessage
import com.witzig.komoot.challenge.dto.NewUserEvent


class Handler : RequestHandler<SQSEvent, Unit> {

    private val objectMapper = jacksonObjectMapper()
    private val newUserService = NewUserService(PushNotificationService(PushMessageCreator()), RecentUserService())

    override fun handleRequest(input: SQSEvent?, context: Context?) {
        input?.let { sqsEvent ->
            sqsEvent
                    .records
                    .forEach {
                        println("Received event: ${it.body}")
                        val newMessageEvent = objectMapper.readValue(it.body, NewMessage::class.java)
                        val newUserEvent = objectMapper.readValue(newMessageEvent.message, NewUserEvent::class.java)
                        newUserService.handleNewUserEvent(newUserEvent)
                    }
        }
    }

}