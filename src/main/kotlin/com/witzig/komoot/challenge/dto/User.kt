package com.witzig.komoot.challenge.dto

data class User(val id: Long, val name: String)
