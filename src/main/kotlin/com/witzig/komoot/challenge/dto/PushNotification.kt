package com.witzig.komoot.challenge.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class PushNotification(
        val sender: String = "thorsten.witzig@posteo.de",
        val receiver: Long,
        val message: String,
        @JsonProperty("recent_user_ids")
        val recentUserIds: List<Long>
)