package com.witzig.komoot.challenge.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
data class NewMessage(
        @JsonProperty("Message")
        val message: String
)

data class NewUserEvent(
        val name: String,
        val id: Long,
        @JsonProperty("created_at")
        val createdAt: String
)
