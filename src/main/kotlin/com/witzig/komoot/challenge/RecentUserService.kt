package com.witzig.komoot.challenge

import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.model.*
import com.witzig.komoot.challenge.dto.User

class RecentUserService {

    private val tableName = "UserTable"
    private val ttlOneHour = 1000 * 60 * 60
    private val dynamoDbClient = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.EU_WEST_1).build()

    fun retrieveThreeRandomRecentUsers(): List<User> {
        val scanRequest = ScanRequest(tableName).withLimit(100)
        val items = dynamoDbClient.scan(scanRequest).items
        println("Found ${items.size} user in db")
        if (items.isEmpty())
            return emptyList()
        items.shuffle()
        return items
                .take(3)
                .map {
                    User(
                            id = it.getValue("userId").s.toLong(),
                            name = it.getValue("userName").s)
                }
    }

    fun addRecentUser(newUser: User) {
        println("Save user ${newUser.name}")
        val ttl = System.currentTimeMillis() + ttlOneHour
        val userItem = mapOf(Pair("userId", AttributeValue(newUser.id.toString())),
                Pair("userName", AttributeValue(newUser.name)),
                Pair("ttl", AttributeValue(ttl.toString())))
        dynamoDbClient.putItem(tableName, userItem)
    }
}