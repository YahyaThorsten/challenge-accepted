package com.witzig.komoot.challenge

import com.witzig.komoot.challenge.dto.PushNotification
import com.witzig.komoot.challenge.dto.User

class PushMessageCreator {

    fun createPushNotification(newUser: User, recentUsers: List<User>) =
            PushNotification(receiver = newUser.id,
                    message = createMessageFor(newUser, recentUsers),
                    recentUserIds = recentUsers.map { it.id })

    private fun createMessageFor(newUser: User, recentUsers: List<User>): String {
        val nameMessagePart = createNameMessagePart(recentUsers)
        return "Hi ${newUser.name}, welcome to komoot.$nameMessagePart"
    }

    private fun createNameMessagePart(recentUsers: List<User>): String {
        val suffix = "also joined recently."
        return when {
            recentUsers.isEmpty() -> {
                ""
            }
            recentUsers.size == 1 -> {
                " ${recentUsers[0].name} $suffix"
            }
            recentUsers.size == 2 -> {
                " ${recentUsers[0].name} and ${recentUsers[1].name} $suffix"
            }
            else -> {
                " ${recentUsers[0].name}, ${recentUsers[1].name} and ${recentUsers[2].name} $suffix"
            }
        }
    }
}