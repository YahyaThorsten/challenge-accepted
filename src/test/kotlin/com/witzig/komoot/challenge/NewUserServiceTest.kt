package com.witzig.komoot.challenge

import com.witzig.komoot.challenge.dto.NewUserEvent
import com.witzig.komoot.challenge.dto.User
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

internal class NewUserServiceTest {

    private val pushNotificationService = mockk<PushNotificationService>()
    private val recentUserService = mockk<RecentUserService>()
    private val newUserService = NewUserService(pushNotificationService = pushNotificationService, recentUserService = recentUserService)

    @Test
    fun handleNewUserEvent() {
        val event = NewUserEvent("Hans", 1,"2020-12-12T14:01:32.055")
        val recentUsers = listOf(User(id = 42, name = "Peter"),
                User(id = 23, name = "Pan"),
                User(id = 32, name = "Paul"))
        val newUser = User(id = 1, name = "Hans")
        every { recentUserService.retrieveThreeRandomRecentUsers() } returns recentUsers
        every { pushNotificationService.sendPushNotification(newUser, recentUsers) } returns true
        every { recentUserService.addRecentUser(newUser) } returns Unit

        newUserService.handleNewUserEvent(event)

        verify { recentUserService.retrieveThreeRandomRecentUsers() }
        verify { pushNotificationService.sendPushNotification(newUser, recentUsers) }
        verify { recentUserService.addRecentUser(newUser) }

    }
}