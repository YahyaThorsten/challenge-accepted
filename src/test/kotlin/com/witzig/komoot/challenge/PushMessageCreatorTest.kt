package com.witzig.komoot.challenge

import com.witzig.komoot.challenge.dto.PushNotification
import com.witzig.komoot.challenge.dto.User
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class PushMessageCreatorTest {

    @Test
    fun createPushNotification() {
        val pushMessageCreator = PushMessageCreator()

        val newUser = User(id = 3, name = "Hans Wurst")
        val recentUsers = listOf(
                User(id = 5,name = "Tick"),
                User(id = 8,name = "Trick"),
                User(id = 13,name = "Track"),
        )
        val notification = pushMessageCreator.createPushNotification(newUser, recentUsers)
        val expectedNotification = PushNotification(receiver =  3,
                recentUserIds = listOf(5,8,13),
                message = "Hi Hans Wurst, welcome to komoot. Tick, Trick and Track also joined recently.")
        assertEquals(notification, expectedNotification)
    }

    @Test
    fun createPushNotificationForTwoRecentUser() {
        val pushMessageCreator = PushMessageCreator()

        val newUser = User(id = 3, name = "Hans Wurst")
        val recentUsers = listOf(
                User(id = 5,name = "Siegfried"),
                User(id = 8,name = "Roy")
        )
        val notification = pushMessageCreator.createPushNotification(newUser, recentUsers)
        val expectedNotification = PushNotification(receiver =  3,
                recentUserIds = listOf(5,8),
                message = "Hi Hans Wurst, welcome to komoot. Siegfried and Roy also joined recently.")
        assertEquals(notification, expectedNotification)
    }

    @Test
    fun createPushNotificationForOneRecentUser() {
        val pushMessageCreator = PushMessageCreator()

        val newUser = User(id = 3, name = "Hans Wurst")
        val recentUsers = listOf(
                User(id = 5,name = "Tom"),
        )
        val notification = pushMessageCreator.createPushNotification(newUser, recentUsers)
        val expectedNotification = PushNotification(receiver =  3,
                recentUserIds = listOf(5),
                message = "Hi Hans Wurst, welcome to komoot. Tom also joined recently.")
        assertEquals(notification, expectedNotification)
    }

    @Test
    fun createPushNotificationForNoRecentUser() {
        val pushMessageCreator = PushMessageCreator()

        val newUser = User(id = 3, name = "Hans Wurst")
        val recentUsers = emptyList<User>()
        val notification = pushMessageCreator.createPushNotification(newUser, recentUsers)
        val expectedNotification = PushNotification(receiver =  3,
                recentUserIds = emptyList(),
                message = "Hi Hans Wurst, welcome to komoot.")
        assertEquals(notification, expectedNotification)
    }
}